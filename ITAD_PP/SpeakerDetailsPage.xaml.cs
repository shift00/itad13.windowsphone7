﻿using Microsoft.Phone.Controls;
using System.Windows.Navigation;
using ITAD_PP.ViewModel;
using ITAD_PP.Model;

namespace ITAD_PP
{
    /// <summary>
    /// Description for SpeakerDetailsPage.
    /// </summary>
    public partial class SpeakerDetailsPage : PhoneApplicationPage
    {
        /// <summary>
        /// Initializes a new instance of the SpeakerDetailsPage class.
        /// </summary>
        public SpeakerDetailsPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string selectedIndex = "";
            if (NavigationContext.QueryString.TryGetValue("selectedItem", out selectedIndex))
            {
                SpeakersViewModel model = new SpeakersViewModel();
                DataContext = model.SpeakersList[int.Parse(selectedIndex)];
            }
        }
    }
}