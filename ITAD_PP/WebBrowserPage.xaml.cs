﻿using Microsoft.Phone.Controls;
using System.Windows.Navigation;
using System.Windows.Controls;

namespace ITAD_PP
{
    /// <summary>
    /// Description for WebBrowserPage.
    /// </summary>
    public partial class WebBrowserPage : PhoneApplicationPage
    {
        /// <summary>
        /// Initializes a new instance of the WebBrowserPage class.
        /// </summary>
        public WebBrowserPage()
        {
            InitializeComponent();
        }

        private void WebBrowser_Navigating(object sender, NavigatingEventArgs e)
        {
            ProgressBar.IsIndeterminate = true;
        }

        private void WebBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            ProgressBar.IsIndeterminate = false;
        }
    }
}