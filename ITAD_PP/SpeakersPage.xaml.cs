﻿using Microsoft.Phone.Controls;

namespace ITAD_PP
{
    /// <summary>
    /// Description for SpeakersPage.
    /// </summary>
    public partial class SpeakersPage : PhoneApplicationPage
    {
        /// <summary>
        /// Initializes a new instance of the SpeakersPage class.
        /// </summary>
        public SpeakersPage()
        {
            InitializeComponent();
        }

        private void SpeakersListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (SpeakersListBox.SelectedIndex == -1)
                return;

            NavigationService.Navigate(new System.Uri("/SpeakerDetailsPage.xaml?selectedItem=" + SpeakersListBox.SelectedIndex, System.UriKind.Relative));

            SpeakersListBox.SelectedIndex = -1;

        }
    }
}