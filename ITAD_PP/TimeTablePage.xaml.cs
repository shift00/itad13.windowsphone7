﻿using Microsoft.Phone.Controls;

namespace ITAD_PP
{
    /// <summary>
    /// Description for TimeTablePage.
    /// </summary>
    public partial class TimeTablePage : PhoneApplicationPage
    {
        /// <summary>
        /// Initializes a new instance of the TimeTablePage class.
        /// </summary>
        public TimeTablePage()
        {
            InitializeComponent();
        }
    }
}