﻿using GalaSoft.MvvmLight;
using System.Collections.Generic;
using ITAD_PP.Model;
using System.Collections.ObjectModel;

namespace ITAD_PP.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class TimeTableViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the TimeTableViewModel class.
        /// </summary>
        
        #region Constructor
        public TimeTableViewModel()
        {
            LoadListData();
        } 
        #endregion

        #region Fields
        private string pageTitle = "Agenda konferencji";
        private ObservableCollection<SessionModel> sessionList;
        #endregion

        #region Method
        public void LoadListData()
        {
            SessionList = new ObservableCollection<SessionModel>();
            SessionList.Add(new SessionModel { Date = "10:00-10:15", Company = "Microsoft Sp. z o.o.", Speaker = "Valerie Anderson", Title = "How to meet company" });
            SessionList.Add(new SessionModel { Date = "10:00-10:15", Company = "Microsoft Sp. z o.o.", Speaker = "Steve Ballmer", Title = "How to meet company" });
            SessionList.Add(new SessionModel { Date = "10:00-10:15", Company = "Microsoft Sp. z o.o.", Speaker = "Valerie Anderson", Title = "How to meet company" });
            SessionList.Add(new SessionModel { Date = "10:00-10:15", Company = "Microsoft Sp. z o.o.", Speaker = "Valerie Anderson", Title = "How to meet company" });
            SessionList.Add(new SessionModel { Date = "10:00-10:15", Company = "Microsoft Sp. z o.o.", Speaker = "Valerie Anderson", Title = "How to meet company" });
            SessionList.Add(new SessionModel { Date = "10:00-10:15", Company = "Microsoft Sp. z o.o.", Speaker = "Valerie Anderson", Title = "How to meet company" });
        }
        #endregion

        #region Properties
        public string PageTitle
        {
            get { return pageTitle; }
            set
            {
                pageTitle = value;
                RaisePropertyChanged("PageTitle");
            }
        }
        public ObservableCollection<SessionModel> SessionList
        {
            get { return sessionList; }
            set { sessionList = value; }
        }
        #endregion

       
        
    }
}