﻿using GalaSoft.MvvmLight;
using System.Collections.Generic;
using ITAD_PP.Model;
using System.Collections.ObjectModel;

namespace ITAD_PP.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>  
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary> 
    public class PartnersViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the PartnersViewModel class.
        /// </summary>
        
        #region Constructor
        public PartnersViewModel()
        {
            LoadPartnersList();
        } 
        #endregion

        #region Fields
        private string pageTitle = "Partnerzy konferencji";
        private ObservableCollection<PartnerModel> partnersList;

        #endregion

        #region Methods
        private void LoadPartnersList()
        {
            PartnersList = new ObservableCollection<PartnerModel>();
            PartnersList.Add(new PartnerModel { Name = "Microsoft sp. z o.o.", Logo = "Assets/Partners/microsoft_png.png" });
            PartnersList.Add(new PartnerModel { Name = "Microsoft sp. z o.o.", Logo = "Assets/Partners/microsoft_png.png" });
        }
        #endregion

        #region Properties
        public string PageTitle
        {
            get { return pageTitle; }
            set
            {
                pageTitle = value;
                RaisePropertyChanged("PageTitle");
            }
        }
        public ObservableCollection<PartnerModel> PartnersList
        {
            get { return partnersList; }
            set { partnersList = value; }
        }
        #endregion
        
    }
}