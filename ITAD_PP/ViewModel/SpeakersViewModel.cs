﻿using GalaSoft.MvvmLight;
using System.Collections.Generic;
using ITAD_PP.Model;
using System.Collections.ObjectModel;

namespace ITAD_PP.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SpeakersViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the SpeakersViewModel class.
        /// </summary>
        
        #region Constructor
        public SpeakersViewModel()
        {
            LoadSpeakersList();
        } 
        #endregion

        #region Fields
        private string pageTitle = "Prelegenci";
        

        #endregion

        #region Methods
        public void LoadSpeakersList()
        {
            SpeakersList = new ObservableCollection<SpeakerModel>();
            
            SpeakersList.Add(new SpeakerModel { Name = "Valerie Anderson", Company = "Microsoft sp. z o.o.", Description = "Litwo! Ojczyzno moja! Ty jesteś jak zdrowie. Ile cię trzeba było ogrodniczki. Tylko co go pilnował i znowu w domu ziemię kochaną i niech Jaśnie Wielmożny Podkomorzy i swoją ważność zarazem poznaje. jak zdrowie. Nazywał się kołem. W zamku sień wielka, jeszcze kołyszą się nieznanej osobie przypomniał, że ją nudzi rzecz długa, choć utrudzony, chociaż w broszurki i widać nóżki na Francuza.", Photo = "Assets/Speakers/template_png.png" });
            SpeakersList.Add(new SpeakerModel { Name = "Valerie Anderson", Company = "Microsoft sp. z o.o.", Description = "Litwo! Ojczyzno moja! Ty jesteś jak zdrowie. Ile cię trzeba było ogrodniczki. Tylko co go pilnował i znowu w domu ziemię kochaną i niech Jaśnie Wielmożny Podkomorzy i swoją ważność zarazem poznaje. jak zdrowie. Nazywał się kołem. W zamku sień wielka, jeszcze kołyszą się nieznanej osobie przypomniał, że ją nudzi rzecz długa, choć utrudzony, chociaż w broszurki i widać nóżki na Francuza.", Photo = "Assets/Speakers/template_png.png" });
            SpeakersList.Add(new SpeakerModel { Name = "Valerie Anderson", Company = "Microsoft sp. z o.o.", Description = "Litwo! Ojczyzno moja! Ty jesteś jak zdrowie. Ile cię trzeba było ogrodniczki. Tylko co go pilnował i znowu w domu ziemię kochaną i niech Jaśnie Wielmożny Podkomorzy i swoją ważność zarazem poznaje. jak zdrowie. Nazywał się kołem. W zamku sień wielka, jeszcze kołyszą się nieznanej osobie przypomniał, że ją nudzi rzecz długa, choć utrudzony, chociaż w broszurki i widać nóżki na Francuza.", Photo = "Assets/Speakers/template_png.png" });
            SpeakersList.Add(new SpeakerModel { Name = "Valerie Anderson", Company = "Microsoft sp. z o.o.", Description = "Litwo! Ojczyzno moja! Ty jesteś jak zdrowie. Ile cię trzeba było ogrodniczki. Tylko co go pilnował i znowu w domu ziemię kochaną i niech Jaśnie Wielmożny Podkomorzy i swoją ważność zarazem poznaje. jak zdrowie. Nazywał się kołem. W zamku sień wielka, jeszcze kołyszą się nieznanej osobie przypomniał, że ją nudzi rzecz długa, choć utrudzony, chociaż w broszurki i widać nóżki na Francuza.", Photo = "Assets/Speakers/template_png.png" });
        }
        #endregion
        
        #region Properties
        public string PageTitle
        {
            get { return pageTitle; }
            set
            {
                pageTitle = value;
                RaisePropertyChanged("PageTitle");
            }
        }
        public ObservableCollection<SpeakerModel> SpeakersList { get; set; }
        #endregion
        
    }
}