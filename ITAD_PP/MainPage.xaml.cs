﻿using Microsoft.Phone.Controls;

namespace ITAD_PP
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void TimeTableButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.Navigate(new System.Uri("/TimeTablePage.xaml", System.UriKind.Relative));
        }

        private void SpeakersButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.Navigate(new System.Uri("/SpeakersPage.xaml", System.UriKind.Relative));
        }

        private void PartnersButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.Navigate(new System.Uri("/PartnersPage.xaml", System.UriKind.Relative));
        }

        private void RegisterButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.Navigate(new System.Uri("/WebBrowserPage.xaml", System.UriKind.Relative));
        }
    }
}
